# Torrentbox

## Pre-flight

```bash
sudo apt-get update; sudo apt autoremove; sudo apt-get dist-upgrade;
```

## Install OpenVPN + PIA

```bash
sudo apt-get install openvpn unzip
```

### Disable IPv6
Add the following to:

```bash
sudo nano /etc/sysctl.conf
```

```bash
net.ipv6.conf.all.disable_ipv6 = 1
```

Run the following and reboot.

```bash
sudo sysctl -p
```

### Download latest server connections

```bash
sudo wget https://www.privateinternetaccess.com/openvpn/openvpn.zip
sudo unzip openvpn.zip
```

### Move server config

```bash
sudo mv CA\ Toronto.ovpn /etc/openvpn/CA_Toronto.ovpn
```

### Add login credentials

```bash
sudo nano /etc/openvpn/login.conf
sudo chmod 400 /etc/openvpn/login.conf
```

Add the following to `sudo nano /etc/default/openvpn`

```
AUTOSTART=pia_toronto
```

### Prevent DNS leaks

```bash
sudo apt-get install openresolv
```

Add the following to the server config.

```
script-security 2
up /etc/openvpn/update-resolv-conf
down /etc/openvpn/update-resolv-conf
```

## Mount External Hard Drive

### List all devices

```bash
sudo lsblk -f -o NAME,LABEL,SIZE,UUID,MOUNTPOINT
```

```bash
sudo nano /etc/fstab
```

```bash
LABEL=Passport       /media/passport     ext4    defaults        0       1
```

```bash
sudo mount -a
```

Reboot.

## Install Samba

```bash
sudo apt-get install samba samba-common-bin -y
sudo smbpasswd -a pi
```

Backup the original config.

```bash
sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.orig
```

Add to the bottom of `sudo nano /etc/samba/smb.conf`

```
[passport]
comment = Torrentbox
path = /media/passport
force user = "root"
read only = no
guest ok = yes
public = yes
```

Reboot.

## Install Transmission

```bash
sudo apt-get install transmission-daemon -y
```

